# TYPO3 CMS Boilerplate Project

[![pipeline status](https://gitlab.com/garfieldius/init/badges/master/pipeline.svg)](https://gitlab.com/garfieldius/init/commits/master) [![coverage report](https://gitlab.com/garfieldius/init/badges/master/coverage.svg)](https://gitlab.com/garfieldius/init/commits/master)

This is a skeleton/boilerplate setup for new TYPO3 CMS projects. While a few things are quite opinionated, it tries to follow best practices in most cases.

## Installation

`composer create-project grossberger-georg/typo3-init my-typo3-project`

### Dependencies

* [make](https://www.gnu.org/software/make/)
* [ddev](https://docs.docker.com/install/)

## License

[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)
