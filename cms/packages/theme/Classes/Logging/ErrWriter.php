<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\Theme\Logging;

use TYPO3\CMS\Core\Log\LogRecord;
use TYPO3\CMS\Core\Log\Writer\AbstractWriter;

/**
 * Write messages to stderr
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ErrWriter extends AbstractWriter
{
    /**
     * @var resource|null
     */
    private $fh;

    public function __destruct()
    {
        if ($this->fh && is_resource($this->fh)) {
            fclose($this->fh);
        }
    }

    public function writeLog(LogRecord $record)
    {
        if (!$this->fh) {
            $this->fh = fopen('php://stderr', 'wb');
        }

        $timestamp = date('r', (int) $record->getCreated());
        $levelName = strtoupper($record->getLevel());

        $message = sprintf(
            '%s [%s] %s' . PHP_EOL,
            $timestamp,
            $levelName,
            $record->getMessage()
        );

        fwrite($this->fh, $message);
    }
}
