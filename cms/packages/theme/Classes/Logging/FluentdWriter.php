<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\Theme\Logging;

use Fluent\Logger\FluentLogger;
use TYPO3\CMS\Core\Log\LogRecord;
use TYPO3\CMS\Core\Log\Writer\AbstractWriter;

/**
 * Log writer for fluentd
 *
 * @author Georg Großberger <g.grossberger@supseven.at>
 */
class FluentdWriter extends AbstractWriter
{
    use MonologConverter;

    /**
     * @var string
     */
    private string $host = 'fluentd';

    /**
     * @var int
     */
    private int $port = 24224;

    /**
     * @var string
     */
    private string $tag = 'app.typo3';

    /**
     * @var FluentLogger
     */
    private FluentLogger $logger;

    public function __construct(array $options = [])
    {
        parent::__construct($options);
        $this->logger = new FluentLogger($this->host, $this->port);
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @param int $port
     */
    public function setPort(int $port): void
    {
        $this->port = $port;
    }

    /**
     * @param string $tag
     */
    public function setTag(string $tag): void
    {
        $this->tag = $tag;
    }

    public function writeLog(LogRecord $record)
    {
        $this->logger->post($this->tag, $this->recordToArray($record));
    }
}
