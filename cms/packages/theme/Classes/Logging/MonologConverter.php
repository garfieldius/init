<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\Theme\Logging;

use Monolog\Logger;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogRecord;

/**
 * MonologConverter
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
trait MonologConverter
{
    private static array $levelMap = [
        LogLevel::DEBUG     => Logger::DEBUG,
        LogLevel::INFO      => Logger::INFO,
        LogLevel::NOTICE    => Logger::NOTICE,
        LogLevel::WARNING   => Logger::WARNING,
        LogLevel::ERROR     => Logger::ERROR,
        LogLevel::CRITICAL  => Logger::CRITICAL,
        LogLevel::ALERT     => Logger::ALERT,
        LogLevel::EMERGENCY => Logger::EMERGENCY,
    ];

    protected function recordToArray(LogRecord $record): array
    {
        return [
            'message'   => $record->getMessage(),
            'level'     => self::$levelMap[$record->getLevel()],
            'severity'  => $record->getLevel(),
            'datetime'  => $record->getCreated(),
            'timestamp' => date('r', (int) $record->getCreated()),
            'channel'   => $record->getComponent(),
            'context'   => [
                'request'   => $record->getRequestId(),
                'component' => $record->getComponent(),
                'sapi'      => PHP_SAPI,
            ],
            'extra' => $record->getData(),
        ];
    }
}
