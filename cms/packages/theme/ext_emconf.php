<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

$EM_CONF[$_EXTKEY] = [
    'title'            => 'Project theme',
    'description'      => 'Actual integration of the project',
    'version'          => '1.0.0',
    'state'            => 'stable',
    'category'         => 'misc',
    'clearCacheOnLoad' => 0,
    'constraints'      => [
        'depends' => [
            'base_minimal' => '',
            'json_content' => '',
        ],
        'conflicts' => [],
        'suggests'  => [],
    ],
];
