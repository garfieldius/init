<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

use TYPO3\CMS\Core\Core\Environment;

$path = realpath(__DIR__) . DIRECTORY_SEPARATOR;

// always load Production and at first
$searches = ['Production'];
$prev = '';

// Build load schedule based on context
// Additional contexts overwrite and extend the Production context
foreach (explode('/', (string) Environment::getContext()) as $ctx) {
    if ($prev) {
        $prev .= '_';
    }

    $prev .= $ctx;
    $searches[] = $prev;
}

// Load in given order
foreach (array_unique($searches) as $search) {
    $filePath = $path . $search . '.php';

    if (is_file($filePath)) {
        require_once $filePath;
    }
}
