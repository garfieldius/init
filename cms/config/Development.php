<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

use TYPO3\CMS\Core\Cache\Backend\NullBackend;
use TYPO3\CMS\Core\Cache\Backend\TransientMemoryBackend;
use TYPO3\CMS\Core\Cache\Frontend\PhpFrontend;
use TYPO3\CMS\Core\Cache\Frontend\VariableFrontend;

$GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive($GLOBALS['TYPO3_CONF_VARS'], [
    'SYS' => [
        'devIPmask'           => '*',
        'displayErrors'       => 1,
        'exceptionalErrors'   => E_ALL & ~(E_NOTICE | E_STRICT | E_WARNING | E_DEPRECATED | E_USER_DEPRECATED),
        'trustedHostsPattern' => '.*',
    ],
    'BE' => [
        'debug'         => true,
        'languageDebug' => false,
    ],
]);

foreach ($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['caches'] as $name => $isPHP) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['options'] = [];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['frontend'] = $isPHP
        ? PhpFrontend::class
        : VariableFrontend::class;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['backend'] = $isPHP
        ? NullBackend::class
        : TransientMemoryBackend::class;
}
