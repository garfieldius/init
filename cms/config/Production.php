<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

use TYPO3\CMS\Core\Cache\Backend\RedisBackend;
use TYPO3\CMS\Core\Cache\Frontend\VariableFrontend;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Session\Backend\RedisSessionBackend;

$ctx = (string) Environment::getContext();

$loadPass = static function (string $varName): string {
    $file = getenv($varName . '_FILE');

    if ($file && is_file($file)) {
        return file_get_contents($file);
    }

    return (string) getenv($varName);
};

$GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive($GLOBALS['TYPO3_CONF_VARS'], [
    'BE' => [
        'debug'              => false,
        'explicitADmode'     => 'explicitAllow',
        'languageDebug'      => false,
        'lockIP'             => 4,
        'lockSSL'            => true,
        'loginSecurityLevel' => 'normal',
        'passwordHashing'    => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\Argon2iPasswordHash',
            'options'   => [],
        ],
        'versionNumberInFilename' => true,
    ],
    'DB' => [
        'Connections' => [
            'Default' => [
                'charset'      => 'utf8mb4',
                'dbname'       => getenv('MYSQL_DATABASE'),
                'driver'       => 'mysqli',
                'host'         => getenv('MYSQL_HOSTNAME'),
                'password'     => $loadPass('MYSQL_PASSWORD'),
                'user'         => getenv('MYSQL_USER'),
                'port'         => getenv('MYSQL_PORT'),
                'tableoptions' => [
                    'charset' => 'utf8mb4',
                    'collate' => 'utf8mb4_unicode_ci',
                ],
            ],
        ],
    ],
    'EXTCONF' => [
        'json_content' => [
            'menus' => [
                'main' => [
                    'pid'    => 1,
                    'depth'  => 5,
                    'fields' => [],
                ],
            ],
        ],
    ],
    'EXTENSIONS' => [
        'backend' => [
            'backendFavicon'       => '',
            'backendLogo'          => '',
            'loginBackgroundImage' => '',
            'loginFootnote'        => '',
            'loginHighlightColor'  => '',
            'loginLogo'            => '',
        ],
        'extensionmanager' => [
            'automaticInstallation' => '1',
            'offlineMode'           => '0',
        ],
        'news' => [
            'advancedMediaPreview'                => '1',
            'archiveDate'                         => 'date',
            'categoryBeGroupTceFormsRestriction'  => '0',
            'categoryRestriction'                 => '',
            'contentElementPreview'               => '1',
            'contentElementRelation'              => '1',
            'dateTimeNotRequired'                 => '0',
            'hidePageTreeForAdministrationModule' => '0',
            'manualSorting'                       => '0',
            'mediaPreview'                        => 'false',
            'prependAtCopy'                       => '1',
            'resourceFolderImporter'              => '/news_import',
            'rteForTeaser'                        => '0',
            'showAdministrationModule'            => '1',
            'showImporter'                        => '0',
            'storageUidImporter'                  => '1',
            'tagPid'                              => '1',
        ],
        'scheduler' => [
            'maxLifetime'     => '1440',
            'showSampleTasks' => '1',
        ],
        'solr' => [
            'allowLegacySiteMode'                         => '0',
            'allowSelfSignedCertificates'                 => '0',
            'useConfigurationFromClosestTemplate'         => '0',
            'useConfigurationMonitorTables'               => '',
            'useConfigurationTrackRecordsOutsideSiteroot' => '1',
        ],
        'static_info_tables' => [
            'enableManager' => '0',
        ],
        'tt_address' => [
            'backwardsCompatFormat'            => '%1$s %3$s',
            'readOnlyNameField'                => '1',
            'storeBackwardsCompatName'         => '1',
            'telephoneValidationPatternForJs'  => '/[^\\d\\+\\s\\-]/g',
            'telephoneValidationPatternForPhp' => '/[^\\d\\+\\s\\-]/',
        ],
    ],
    'FE' => [
        'cookieSameSite'                    => 'strict',
        'debug'                             => false,
        'disableNoCacheParameter'           => true,
        'hidePagesIfNotTranslatedByDefault' => true,
        'pageNotFoundOnCHashError'          => false,
        'lockIP'                            => 2,
        'passwordHashing'                   => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\Argon2idPasswordHash',
            'options'   => [],
        ],
        'versionNumberInFilename' => 'embed',
    ],
    'GFX' => [
        'gif_compress'                       => false,
        'processor'                          => 'GraphicsMagick',
        'processor_allowTemporaryMasksAsPng' => true,
        'processor_colorspace'               => 'sRGB',
        'processor_effects'                  => true,
        'processor_enabled'                  => true,
        'processor_path'                     => '/usr/bin/',
        'processor_path_lzw'                 => '/usr/bin/',
    ],
    'MAIL' => [
        'transport'                  => 'smtp',
        'transport_sendmail_command' => '',
        'transport_smtp_encrypt'     => getenv('SMTP_ENCRYPTION'),
        'transport_smtp_password'    => $loadPass('SMTP_PASSWORD'),
        'transport_smtp_server'      => getenv('SMTP_HOST'),
        'transport_smtp_username'    => getenv('SMTP_USER'),
    ],
    'SYS' => [
        'UTF8filesystem' => true,
        'caching'        => [
            'caches' => [
                'core'                     => true,
                'fluid_template'           => true,
                'assets'                   => false,
                'hash'                     => false,
                'extbase'                  => false,
                'imagesizes'               => false,
                'l10n'                     => false,
                'pages'                    => false,
                'pagesection'              => false,
                'rootline'                 => false,
                'cache_news_category'      => false,
                'tx_solr'                  => false,
                'tx_solr_configuration'    => false,
                'cache_ttaddress_category' => false,
                'ttaddress_geocoding'      => false,
                'base_minimal'             => false,
                'json_content'             => false,
            ],
        ],
        'exceptionalErrors' => E_ALL & ~(E_NOTICE | E_STRICT | E_WARNING | E_DEPRECATED | E_USER_DEPRECATED),
        'features'          => [
            'felogin.extbase'                           => true,
            'fluidBasedPageModule'                      => false,
            'form.legacyUploadMimeTypes'                => false,
            'rearrangedRedirectMiddlewares'             => true,
            'redirects.hitCount'                        => true,
            'security.frontend.keepSessionDataOnLogout' => false,
        ],
        'cookieSecure'      => 1,
        'devIPmask'         => '',
        'displayErrors'     => 0,
        'sitename'          => 'TYPO3 Init - ' . htmlspecialchars($ctx),
        'systemLocale'      => 'de_AT.utf8',
        'systemMaintainers' => [
            1,
        ],
    ],
]);

$redisHost = (string) getenv('REDIS_HOST');
$redisPort = (int) getenv('REDIS_PORT');

if ($redisHost && $redisPort) {
    $auth = (string) getenv('REDIS_AUTH');
    $database = (int) (getenv('REDIS_STARTDB') ?: 2);

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['session'] = [
        'BE' => [
            'backend' => RedisSessionBackend::class,
            'options' => [
                'database' => $database++,
                'hostname' => $redisHost,
                'port'     => $redisPort,
                'password' => $auth,
            ],
        ],
        'FE' => [
            'backend' => RedisSessionBackend::class,
            'options' => [
                'database' => $database++,
                'hostname' => $redisHost,
                'port'     => $redisPort,
            ],
        ],
    ];

    foreach ($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['caches'] as $name => $isPHP) {
        if (!$isPHP) {
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name] = [
                'frontend' => VariableFrontend::class,
                'backend'  => RedisBackend::class,
                'options'  => [
                    'hostname' => $redisHost,
                    'port'     => $redisPort,
                    'database' => $database++,
                ],
                'groups' => $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$name]['groups'] ?? [],
            ];
        }
    }
}

$GLOBALS['TYPO3_CONF_VARS']['LOG'] = require(__DIR__ . '/Logging.php');
