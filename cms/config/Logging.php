<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

use GrossbergerGeorg\Theme\Logging\ErrWriter;
use GrossbergerGeorg\Theme\Logging\FluentdWriter;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Log\LogLevel;

return (function () {
    if (Environment::getContext()->isProduction()) {
        $level = LogLevel::WARNING;
    } else {
        $level = LogLevel::DEBUG;
    }

    if (getenv('FLUENTD_HOST') && !getenv('DISABLE_LOGGER')) {
        $writer = [
            FluentdWriter::class => [
                'host' => (string) getenv('FLUENTD_HOST'),
                'port' => (int) getenv('FLUENTD_PORT'),
            ],
        ];
    } else {
        $level = LogLevel::ERROR;
        $writer = [
            ErrWriter::class => [],
        ];
    }

    $setup = [
        $level => $writer,
    ];

    $loggers = [];
    $channels = [
        [],
        ['TYPO3'],
        ['deprecations'],
        ['ApacheSolrForTypo3'],
        ['ApacheSolrForTypo3', 'Solr'],
    ];

    foreach ($channels as $channel) {
        $parent = &$loggers;

        while (count($channel)) {
            $key = array_shift($channel);

            if (empty($parent[$key])) {
                $parent[$key] = [];
            }

            $parent = &$parent[$key];
        }

        $parent['writerConfiguration'] = $setup;
    }

    return $loggers;
})();
