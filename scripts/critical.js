#!/usr/bin/env node

/*
 * This file is (c) 2020 by Georg Großberger
 *
 * It is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

const { generate } = require("critical");
const { writeFile } = require("fs");
const { basename } = require("path");
const { promisify } = require("util");
const write = promisify(writeFile);
const glob = promisify(require("glob"));

(async function () {
    try {
        const cssFiles = await glob('./public/assets/css/*.css', { nosort: true, absolute: true });
        const htmlFiles = await glob('./src/html/**/*.html', { nosort: true, absolute: true });

        while (htmlFiles.length) {
            const f = htmlFiles.shift();
            const css = await generate({
                inline: false,
                css: cssFiles,
                src: f,
                timeout: 30000
            });

            const targetFile =
                './public/assets/css/' +
                basename(f).replace(/\.html$/, '').toLowerCase() +
                '.css';

            await write(targetFile, css);
        }
    } catch (e) {
        console.error(e);
        process.exit(1);
    }
})();
