#!/usr/bin/env node

const { readFileSync } = require('fs');
const { resolve } = require('path');

const file = resolve(__dirname, '..', 'web', 'package.json');
const data = JSON.parse(readFileSync(file));

const runtime = Object.keys(data.dependencies).join(' ');
const develop = Object.keys(data.devDependencies).join(' ');

console.log('yarn remove ' + runtime + ' ' + develop);
console.log('yarn add ' + runtime);
console.log('yarn add --dev ' + develop);
