#!/usr/bin/env node

/*
 * This file is (c) 2020 by Georg Großberger
 *
 * It is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

const { pipeline } = require("stream");
const fs = require("fs");
const zlib = require("zlib");
const glob = require("glob");
const { parallel } = require("async");

function checkErr(err) {
    if (err) {
        console.error(err);
        process.exit(1);
    }
}

glob("public/assets/**/*.{js,css,svg}", { nosort: true, absolute: true }, (err, files) => {
    checkErr(err);
    const queue = [];

    files.forEach((file) => {
        queue.push(cb => {
            const opts = {
                chunkSize: 32 * 1024,
                level: zlib.Z_BEST_COMPRESSION,
                memLevel: 9,
                windowBits: 15
            };
            pipeline(
                fs.createReadStream(file),
                zlib.createGzip(opts),
                fs.createWriteStream(file + ".gz"),
                cb
            );
        });
        queue.push(cb => {
            const opts = {
                chunkSize: 32 * 1024,
                params: {
                    [zlib.constants.BROTLI_PARAM_MODE]: zlib.constants.BROTLI_MODE_TEXT,
                    [zlib.constants.BROTLI_PARAM_QUALITY]: zlib.constants.BROTLI_MAX_QUALITY,
                    [zlib.constants.BROTLI_PARAM_SIZE_HINT]: fs.statSync(file).size,
                    [zlib.constants.BROTLI_PARAM_LGWIN]: zlib.constants.BROTLI_MAX_WINDOW_BITS
                }
            };

            pipeline(
                fs.createReadStream(file),
                zlib.createBrotliCompress(opts),
                fs.createWriteStream(file + ".br"),
                cb
            );
        })
    });

    parallel(queue, checkErr)
});
