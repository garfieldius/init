#!/usr/bin/env php
<?php
declare(strict_types=1);

/*
 * This file is (c) 2020 by Georg Großberger
 *
 * It is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

// Minimal configuration with an encryption key and a dummy install tool password
$encryptionKey = getenv('TYPO3_ENCRYPTION_KEY') ?: bin2hex(random_bytes(48));
$password = getenv('TYPO3_INSTALL_TOOL_PASSWORD') ?: 'joh316';
$config = [
    'BE' => [
        'installToolPassword' => password_hash($password, PASSWORD_ARGON2I, [
            'memory_cost' => 16384,
            'time_cost'   => 16,
            'threads'     => 2,
        ]),
        'loginSecurityLevel' => 'normal',
    ],
    'SYS' => [
        'trustedHostsPattern' => '.*',
        'encryptionKey'       => $encryptionKey,
        'exceptionalErrors'   => E_ALL & ~(E_STRICT | E_NOTICE | E_DEPRECATED | E_USER_DEPRECATED | E_WARNING),
    ],
    'FE' => [
        'loginSecurityLevel' => 'normal',
    ],
];

// Write the file
$code = sprintf("<?php\nreturn %s;\n", var_export($config, true));

echo $code;
