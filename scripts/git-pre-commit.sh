#!/usr/bin/env bash

# This file is (c) 2020 by Georg Großberger
#
# It is free software; you can redistribute it and/or
# modify it under the terms of the Apache License 2.0
#
# For the full copyright and license information see
# <https://www.apache.org/licenses/LICENSE-2.0>

# Collect changed and staged PHP files
CHANGED=$(git diff --cached --name-only --diff-filter=ACMR HEAD | egrep '\.php' | egrep -v 'ext_localconf|ext_tables' | xargs printf '%s ' | sed -e 's/[[:space:]]*$//')

# Run php-cs-fixer on them, add changed to stage
if [ "${CHANGED}" != "" ]; then
    php ./cms/bin/php-cs-fixer fix -vvv --config=./cms/.php_cs ${CHANGED}
    git add ${CHANGED}
fi
