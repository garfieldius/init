
# (c) 2020 Georg Großberger <contact@grossberger-ge.org>
#
# This file is free software; you can redistribute it and/or
# modify it under the terms of the Apache License 2.0
#
# For the full copyright and license information see
# <https://www.apache.org/licenses/LICENSE-2.0>

PHP ?= $(shell which php)
YARN ?= $(shell which yarn)
COMPOSER ?= $(shell which composer)
TAR ?= $(shell which tar)
NODE ?= $(shell which node)

# PHP 7.4+ is required
ifeq (, $(PHP))
	$(error "php not found")
endif

php_is_new = $(shell $(PHP) -r 'echo (float)phpversion() >= 7.4 ? "yes" : "";')

ifneq ($(findstring yes,$(php_is_new)),yes)
	$(error "php is too old, needs 7.4+")
endif

# composer is required
ifeq (, $(COMPOSER))
	$(error "composer not found")
endif

# yarn is required
ifeq (, $(YARN))
	$(error "yarn not found")
endif

# tar or compatible is required
ifeq (, $(TAR))
	$(error "tar not found")
endif

# node 12+ is required
ifeq (, $(NODE))
	$(error "node.js not found")
endif

node_is_new = $(shell node -e 'console.log(parseInt(process.version.substr(1)) >= 12 ? "yes" : "")')

ifneq ($(findstring yes,$(node_is_new)),yes)
	$(error "node.js is too old, needs v12+")
endif

# Include .env variables
include .env
$(eval $(grep -E '^[A-Z]+' .env | sed 's,^,export ,g'))

# Define some helper macros
define msg
	@printf "\033[32m$(1)\033[0m\n"
endef
define wait
	@printf "\033[32m$(1)\033[0m ... "
endef
define done
	@printf "\033[36mDone\033[0m\n"
endef
define dd_cmd
	@ddev exec cms/bin/typo3cms $(1)
endef

# Defining flags, depending on the build target
COMPOSER_FLAGS = -d cms/ --no-suggest -n --no-progress
NODE_ENV = development

# When calling a dist target, we
# create a production ready build
ifneq ($(findstring dist,$(MAKECMDGOALS)),)
    COMPOSER_FLAGS += -o --no-dev
    NODE_ENV = production
endif

FRONTEND_BUILD =
FRONTEND_SOURCES = $(shell find web/src -type f -name '*.ts' -or -name '*.scss')
ASSET_SOURCES = $(shell find web/static -type f)
ASSET_TARGETS = $(patsubst web/static/%, cms/public/assets/%, $(ASSET_SOURCES))


# Default target "help" prints important goals
# with the short comment after their name
.PHONY: help
help:
	@echo ""
	@echo "  Usage: make TARGET"
	@echo ""
	@echo ""
	@echo "  Available targets:"
	@echo ""
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[36m%-7s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: cms/vendor/autoload.php  ## Run Unittests
	@$(PHP) cms/bin/phpunit -c cms/phpunit.xml

.PHONY: list
lint: cms/vendor/autoload.php  ## Run code linters
	@$(PHP) cms/bin/php-cs-fixer --config=cms/.php_cs fix --verbose --diff --dry-run

.PHONY: fix
fix: cms/vendor/autoload.php  ## Fix code styles
	@$(PHP) cms/bin/php-cs-fixer --config=cms/.php_cs fix --verbose

.PHONY: install
install: build hooks \
	cms/private/typo3conf/LocalConfiguration.php \
	cms/private/typo3conf/ENABLE_INSTALL_TOOL \
	cms/private/fileadmin/user_upload/_temp_/index.html

.PHONY: start
start: install  ## Install and start containers
	@$(call wait,Starting containers)
	@ddev start > /dev/null
	@$(call done)

.PHONY: migrate
migrate: start ## Install, start and migrate
	@$(call wait,Importing database)
	@ddev import-db --src=.ddev/typo3.sql.gz > /dev/null
	@$(call done)
	@$(call wait,Updating DB Schema)
	@$(call dd_cmd,database:updateschema -q)
	@$(call done)
	@$(call wait,Fixing folder structure)
	@$(call dd_cmd,install:fixfolderstructure -q)
	@$(call done)

.PHONY: stop
stop: ## Stop the environment
	@$(call wait,Stopping containers)
	@ddev stop > /dev/null
	@$(call done)

.PHONY: dump
dump: ## Update the DB dump and assets archive
	@ddev export-db -f .ddev/typo3.sql.gz
	@$(TAR) -czf .ddev/typo3.tar.gz -C cms/private fileadmin

.PHONY: dist
dist: dist-typo3 dist-web ## Build production optimized and create artifacts

.PHONY: build
build: typo3 web ## Install dependencies and build

.PHONY: typo3
typo3: \
	cms/vendor/autoload.php \
	cms/private/typo3conf/LocalConfiguration.php \
	cms/private/typo3conf/AdditionalConfiguration.php \
	cms/private/typo3conf/PackageStates.php

.PHONY: dist-typo3
dist-typo3: docker/php/artifact.tar

.PHONY: dist-web
dist-web: docker/nginx/artifact.tar

##
## Distribution artifacts
## Will be consumed by docker build
##

docker/nginx/artifact.tar: typo3 web
	@$(call wait,Compiling web artifact)
	@$(TAR) -cf docker/nginx/artifact.tar -C cms --dereference public
	@$(call done)

docker/php/artifact.tar: typo3 web
	@$(call wait,Compiling TYPO3 artifact)
	@$(TAR) -cf docker/php/artifact.tar -C cms/ bin config packages public private var vendor
	@$(call done)

##
## TYPO3 targets
##

cms/private/typo3conf/PackageStates.php: cms/private/typo3conf/LocalConfiguration.php cms/vendor/autoload.php cms/bin/typo3cms cms/composer.lock cms/composer.json
	@$(call wait,Generating PackageStates.php)
	@DISABLE_LOGGER=1 $(PHP) cms/bin/typo3cms install:generatepackagestates -q
	@touch cms/private/typo3conf/PackageStates.php
	@$(call done)

cms/private/typo3conf/AdditionalConfiguration.php:
	@$(call wait,Linking AdditionalConfiguration.php)
	@mkdir -p cms/private/typo3conf
	@cd cms/private/typo3conf && ln -sf ../../config/Entrypoint.php AdditionalConfiguration.php
	@$(call done)

cms/vendor/autoload.php cms/bin/typo3cms: cms/composer.json cms/composer.lock
	@$(COMPOSER) install $(COMPOSER_FLAGS)
	@touch cms/vendor/autoload.php cms/bin/typo3cms

cms/private/typo3conf/LocalConfiguration.php: cms/private/typo3conf/writeable/LocalConfiguration.php
	@cd cms/private/typo3conf && ln -sf writeable/LocalConfiguration.php

cms/private/typo3conf/writeable/LocalConfiguration.php:
	@$(call wait,Generating LocalConfiguration.php)
	@mkdir -p cms/private/typo3conf/writeable
	@$(PHP) scripts/generate-localconfiguration.php > cms/private/typo3conf/writeable/LocalConfiguration.php
	@$(call done)

cms/private/typo3conf/ENABLE_INSTALL_TOOL:
	@$(call wait,Enabling install tool)
	@mkdir -p cms/private/typo3conf
	@printf "KEEP_FILE" > cms/private/typo3conf/ENABLE_INSTALL_TOOL
	@$(call done)

cms/private/fileadmin/user_upload/_temp_/index.html:
	@$(call wait,Unpacking fileadmin data)
	@mkdir -p cms/private/fileadmin
	@$(TAR) -xf .ddev/typo3.tar.gz -C cms/private
	@touch cms/private/fileadmin/user_upload/_temp_/index.html
	@$(call done)

##
## Web frontend specific targets
##

web: web/node_modules/.yarn-integrity

web/node_modules/.yarn-integrity: web/yarn.lock web/package.json
	@$(YARN) --cwd web install --frozen-lockfile
	@touch web/node_modules/.yarn-integrity

##
## Installation of local development helpers
##

hooks: .git/hooks/pre-commit

.git/hooks/pre-commit: scripts/git-pre-commit.sh
	@$(call wait,Installing git pre-commit hook)
	@cp scripts/git-pre-commit.sh .git/hooks/pre-commit
	@touch .git/hooks/pre-commit
	@$(call done)
